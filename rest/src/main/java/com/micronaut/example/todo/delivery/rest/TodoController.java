package com.micronaut.example.todo.delivery.rest;


import javax.transaction.Transactional;

import com.micronaut.example.todo.Todo;
import com.micronaut.example.todo.persistence.TodoRepository;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;

@Controller("/todos")
class TodoController {

    private final TodoRepository todoRepository;

    TodoController(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Transactional
    @Get
    public Iterable<Todo> getAll() {
        return todoRepository.findAll();
    }

    @Transactional
    @Get("/{id}")
    public Todo get(Long id) {
        return todoRepository.findById(id).orElse(null);
    }
    
    @Transactional
    @Post
	public Todo addTodo(@Body Todo todo) {
		return todoRepository.save(todo);
	}
    
    @Transactional
    @Put("/{id}")
	public Todo updateTodo(Long id, @Body Todo todo) {
    	Todo updateTodo=todoRepository.findById(id).orElse(null);
    	if(updateTodo!=null) {
    		updateTodo.setTitle(todo.getTitle());
    		updateTodo.setCreationDate(todo.getCreationDate());
    		updateTodo.setDoneDate(todo.getDoneDate());
    	}
    	return todoRepository.update(updateTodo);
	}
    
    @Transactional
    @Delete("/{id}")
	public void deleteTodo(Long id) {
    	todoRepository.deleteById(id);
	}

}
