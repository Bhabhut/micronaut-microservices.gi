# Micronaut parent

The purpose of this module is to easily add [Micronaut](https://micronaut.io/) support to your **Maven projects**.
If you want to add Micronaut support in your Maven project, you need just to set this module as the parent of your project `pom.xml`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

     <parent>
        <groupId>io.micronaut</groupId>
        <artifactId>micronaut-parent</artifactId>
        <version>3.2.0</version>
    </parent>

    <groupId>com.micronaut.example</groupId>
    <artifactId>micronaut-microservice</artifactId>
    <version>0.0.1</version>
    ...
```
## Add Module
```
 <modules>
        <module>domain</module>
        <module>rest</module>
 </modules>
```

## To use module in another Module

example: here we are using domain module in rest module so we are adding domain dependency to rest module

```
<dependency>
            <groupId>com.micronaut.example</groupId>
            <artifactId>micronaut-microservice-domain</artifactId>
            <version>${project.version}</version>
</dependency>
```

## Typical Micronaut plugins

There are several Maven plugins that are usually used with Micronaut. If you want to use some already configured plugins you just need to activate it in your project `pom.xml`.

For example to generate an auto-executable jar you can use:

```xml
    <properties>
        ...
        <exec.mainClass>com.example.Application</exec.mainClass>
        ...
    </properties>
```


# Run Example

 - Ability to run application from Maven command line goto rest directory
  then run (`mvn exec:exec`) or if you having Micronaut CLI installed in you machine 
  then you can also run with (`mvn mn:run`).
 To build (compile, test, package, integration tests...) the complete project you can run in the root project directory:
 
 ```bash
mvn clean install
```
