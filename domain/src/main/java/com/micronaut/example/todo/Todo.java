package com.micronaut.example.todo;

import io.micronaut.core.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "todo")
public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    @Column(name = "title")
    public  @NonNull String title;
    @Column(name = "creation_date")
    public  @NonNull LocalDateTime creationDate;
    @Column(name = "done_date")
    public  @Nullable LocalDateTime doneDate;

}
