package com.micronaut.example.todo.persistence;

import com.micronaut.example.todo.Todo;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {

}
